// Fill out your copyright notice in the Description page of Project Settings.


#include "InventorySlotWidget.h"
#include <Kismet/GameplayStatics.h>
#include "MainCharacter.h"

void UInventorySlotWidget::SetEquippedItem()
{
	AMainCharacter* Char = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Char)
	{
		Char->SetEquippedItem(ItemTexture);
	}
}

void UInventorySlotWidget::SetItemTexture(ABasePickup * Item)
{
	(Item) ? ItemTexture = Item->GetPickupTexture() : ItemTexture = nullptr;
}
