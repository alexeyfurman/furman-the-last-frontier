// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Components/StaticMeshComponent.h>
#include "BasePickup.generated.h"

UCLASS()
class THELASTFRONTIER_API ABasePickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickup();

	void SetGlowEffect(bool Status);

	FORCEINLINE UTexture2D* GetPickupTexture() { return PickupTexture; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* PickupSM;

	UPROPERTY(EditAnywhere, Category = "PickupProperties")
		UTexture2D* PickupTexture;

	UPROPERTY(EditAnywhere, Category = "PickupProperties")
		FString ItemName;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
