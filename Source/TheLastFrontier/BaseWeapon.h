// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include <Animation/AnimMontage.h>
#include <GameFramework/Character.h>
#include "ImpactEffectComponent.h"
#include "BaseWeapon.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_DELEGATE_OneParam(FHitResultDelegate, FHitResult, Hit);

UCLASS()
class THELASTFRONTIER_API ABaseWeapon : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	ABaseWeapon();

	virtual void StartAttack();

	virtual void StopAttack();

	UFUNCTION()
		void WeaponCollisionOn();

	UFUNCTION()
		void WeaponCollisionOff();

	UPROPERTY()
		FHitResultDelegate OnHitAttack;

protected:

	UFUNCTION()
		void Attack();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Collision")
		class UCapsuleComponent* SwordCollisionCapsule;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UImpactEffectComponent* ImpactEffectComponent;

	UPROPERTY()
		class ACharacter* WeaponOwner;

	virtual void BeginPlay() override;

public:

	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	
};
