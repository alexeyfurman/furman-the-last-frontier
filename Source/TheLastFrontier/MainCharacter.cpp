// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacter.h"
#include <Camera/CameraComponent.h>
#include <Components/InputComponent.h>
#include <Components/CapsuleComponent.h>
#include <ConstructorHelpers.h>
#include <RotationMatrix.h>
#include <BaseWeapon.h>
#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/Character.h>
#include <GameFramework/Controller.h>
#include <GameFramework/Pawn.h>
#include <GameFramework/SpringArmComponent.h>
#include <MyPlayerController.h>
#include "Kismet/GameplayStatics.h"
#include <Engine/World.h>
#include "VitalityComponent.h"
#include "ManaComponent.h"
#include "XpComponent.h"
#include <Engine/EngineTypes.h>
#include <UnrealString.h>
#include <UnrealMathUtility.h>
#include <GameFramework/Actor.h>

// Sets default values
AMainCharacter::AMainCharacter()
{
	//Set size of collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.f);

	//Set turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;
	
	//Don't rotate when the controller rotates
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	VitalityComponent = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComponent"));

	ManaComponent = CreateDefaultSubobject<UManaComponent>(TEXT("ManaComponent"));

	XpComponent = CreateDefaultSubobject<UXpComponent>(TEXT("XpComponent"));

	//Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);

	//Create & configure spring arm component
	SpringArmComponent = CreateAbstractDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);
	SpringArmComponent->TargetArmLength = 400.0f;
	SpringArmComponent->bUsePawnControlRotation = true;

	//Create & configure camera component
	CameraComponent = CreateAbstractDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;

	//Setting mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkelMesh(TEXT("SkeletalMesh'/Game/WildTribe/Meshes/SK_WildTribes_SetA_Player.SK_WildTribes_SetA_Player'"));

	if (SkelMesh.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SkelMesh.Object);
	}

	//Rotating mesh

	//GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));

	/*SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bInheritYaw = false;*/

	VitalityComponent->OnOwnerDied.AddDynamic(this, &AMainCharacter::CharacterDied);

	RightFootCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RightFootCollisionBox"));
	RightFootCollisionBox->SetupAttachment(RootComponent);
	RightFootCollisionBox->SetCollisionProfileName("NoCollision");
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	ThighWeapon = SpawnWeapon();
	AttachWeaponToThigh(ThighSocket);

	//Attach collision components to sockets
	const FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false);

	RightFootCollisionBox->AttachToComponent(GetMesh(), AttachmentRules, "kick_rSocket");

	LastItemSeen = nullptr;

	Inventory.SetNum(MaxInventoryItems);
	
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Raycast();

}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AMainCharacter::MoveRight);
	PlayerInputComponent->BindAction(TEXT("Attack"), IE_Pressed, this, &AMainCharacter::Attack);
	PlayerInputComponent->BindAction(TEXT("SecondaryAttack"), IE_Pressed, this, &AMainCharacter::SecondaryAttack);
	PlayerInputComponent->BindAction(TEXT("Kick"), IE_Pressed, this, &AMainCharacter::KickInput);
	PlayerInputComponent->BindAction(TEXT("Heal"), IE_Pressed, this, &AMainCharacter::HealInput);
	PlayerInputComponent->BindAction(TEXT("Run"), IE_Pressed, this, &AMainCharacter::StartRun);
	PlayerInputComponent->BindAction(TEXT("Run"), IE_Released, this, &AMainCharacter::StopRun);
	PlayerInputComponent->BindAction(TEXT("EquipWeapon"), IE_Pressed, this, &AMainCharacter::EquipWeapon);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAction(TEXT("Pickup"), IE_Pressed, this, &AMainCharacter::PickupItem);
	PlayerInputComponent->BindAction(TEXT("Inventory"), IE_Pressed, this, &AMainCharacter::HandleInventoryInput);

	/*FInputActionBinding InventoryBinding;

	InventoryBinding.bExecuteWhenPaused = true;
	InventoryBinding.ActionDelegate.BindDelegate(this, FName("HandleInventoryInput"));
	InventoryBinding.GetActionName() = FName("Inventory");
	InventoryBinding.KeyEvent = IE_Pressed;

	PlayerInputComponent->AddActionBinding(InventoryBinding)*/;
}




void AMainCharacter::MoveForward(float AxisValue)
{
	if ((Controller != NULL) && (AxisValue != 0.0f))
	{
		//Find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		//Get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		//Add movement in that direction
		AddMovementInput(Direction, AxisValue);
	}
}

void AMainCharacter::MoveRight(float AxisValue)
{
	if ((Controller != NULL) && (AxisValue != 0.0f))
	{
		//Find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		//Get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		//Add movement in that direction
		AddMovementInput(Direction, AxisValue);
	}
}

void AMainCharacter::Attack()
{
	if (bWeaponIsEquipped == true && GetCurrentMontage() == NULL) {
		if (ThighWeapon)
		{
			ThighWeapon->StartAttack();
			PlayAnimMontage(AttackMontage);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Not Current Weapon"));
		}
	}
}

void AMainCharacter::SecondaryAttack()
{
	if (bWeaponIsEquipped == true && GetCurrentMontage() == NULL) {
		if (ThighWeapon)
		{
			ThighWeapon->StartAttack();
			PlayAnimMontage(SecondaryAttackMontage);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Not Current Weapon"));
		}
	}
}

void AMainCharacter::KickInput()
{
	if (GetCurrentMontage() == NULL)
	{
		PlayAnimMontage(KickMontage);
	}
}

void AMainCharacter::KickStart()
{
	UE_LOG(LogTemp, Warning, TEXT("Changing right foot collision to weapon"));
	RightFootCollisionBox->SetCollisionProfileName("Weapon");
}

void AMainCharacter::KickEnd()
{
	UE_LOG(LogTemp, Warning, TEXT("Changing right foot collision to no collision"));
	RightFootCollisionBox->SetCollisionProfileName("NoCollision");
}


void AMainCharacter::HealInput()
{
	if (GetCurrentMontage() == NULL && VitalityComponent->GetCurrentHealth() != 100)
	{
		PlayAnimMontage(HealMontage);
	}
}

void AMainCharacter::HealStart()
{
	//if (OtherActor->GetComponentByClass(UVitalityComponent::StaticClass()))
	//{
	//	UVitalityComponent* Health = Cast<UVitalityComponent>(OtherActor->GetComponentByClass(UVitalityComponent::StaticClass()));
	//	float HealthAfterHeal = FMath::Min(Health->GetCurrentHealth() + HealthHealAmount, Health->GetMaxHealth());
	//	Health->SetCurrentHealth(HealthAfterHeal);
	//	UE_LOG(LogTemp, Warning, TEXT("Health Start: Heal should have worked"));
	//}

	VitalityComponent->AddHealth(HealthHealAmount);
	ManaComponent->SubstractMana(ManaToHealAmount);
	UE_LOG(LogTemp, Warning, TEXT("Health Start: Heal should have worked"));
	GetCharacterMovement()->DisableMovement();
}

void AMainCharacter::HealEnd()
{
	/*UVitalityComponent* Health2 = Cast<UVitalityComponent>(OtherActor->GetComponentByClass(UVitalityComponent::StaticClass()));
	float HealthAfterHeal2 = Health2->GetCurrentHealth() + HealthHealAmount;
	Health2->SetCurrentHealth(HealthAfterHeal2);*/

	/*float HealthAfterHeal3 = FMath::Min(VitalityComponent->GetCurrentHealth() + HealthHealAmount, VitalityComponent->GetMaxHealth());
	VitalityComponent->SetCurrentHealth(HealthAfterHeal3);
	UE_LOG(LogTemp, Warning, TEXT("Health End: Heal should have worked"));*/

	/*VitalityComponent->AddHealth(HealthHealAmount);
	UE_LOG(LogTemp, Warning, TEXT("Health End: Heal should have worked"));*/
	GetCharacterMovement()->SetDefaultMovementMode();
}

void AMainCharacter::WeaponCollisionOnCharacter()
{
	ThighWeapon->WeaponCollisionOn();
}

void AMainCharacter::WeaponCollisionOffCharacter()
{
	ThighWeapon->WeaponCollisionOff();
}

void AMainCharacter::SetEquippedItem(UTexture2D * Texture)
{
	if (Texture)
	{
		//The assumption is that every pickup has a unique texture.
		//To set the equipped item we check every item
		//inside our Inventory. Once we find an item that has the same image as the
		//Texture image we're passing as a parameter we mark that item as CurrentlyEquipped.
		for (auto It = Inventory.CreateIterator(); It; It++)
		{
			if ((*It) && (*It)->GetPickupTexture() && (*It)->GetPickupTexture()->HasSameSourceArt(Texture))
			{
				CurrentlyEquippedItem = *It;
				GLog->Log("I've set a new equipped item: " + CurrentlyEquippedItem->GetName());
				break;
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("The Player has clicked an empty inventory slot"));
	}
}

void AMainCharacter::StartRun()
{
	GetCharacterMovement()->MaxWalkSpeed = RunSpeed;
}

void AMainCharacter::StopRun()
{
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
}

void AMainCharacter::EquipWeapon()
{

	if (bWeaponIsEquipped == false)
	{
		PlayAnimMontage(EquipMontage);
		AttachWeaponToHand(WeaponSocket);
		bWeaponIsEquipped = true;
	}
	else if (bWeaponIsEquipped == true)
	{
		PlayAnimMontage(HolsterMontage);
		AttachWeaponToThigh(ThighSocket);
		bWeaponIsEquipped = false;
	}
}

void AMainCharacter::PickupItem()
{
	if (LastItemSeen)
	{
		//Find the first available slot
		int32 AvailableSlot = Inventory.Find(nullptr);

		if (AvailableSlot != INDEX_NONE)
		{
			//Add the item to the first valid slot found
			Inventory[AvailableSlot] = LastItemSeen;
			//Destroy the item from the game
			LastItemSeen->Destroy();
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("You can't carry any more items!"));
		}
	}
}

void AMainCharacter::CharacterDied()
{
	GetCharacterMovement()->DisableMovement();
	bUseControllerRotationYaw = false;
	ThighWeapon = nullptr;
	UE_LOG(LogTemp, Warning, TEXT("Characted Died: Main Character CPP Warning"));
}

void AMainCharacter::HandleInventoryInput()
{
	AMyPlayerController* Con = Cast<AMyPlayerController>(GetController());
	if (Con)
	{
		Con->HandleInventoryInputController();
	}
}

void AMainCharacter::DropEquippedItem()
{
	if (CurrentlyEquippedItem)
	{
		int32 IndexOfItem;
		if (Inventory.Find(CurrentlyEquippedItem, IndexOfItem))
		{
			//The location of the drop
			FVector DropLocation = GetActorLocation() + (GetActorForwardVector() * 200);

			//Making a transform with default rotation and scale. Just setting up the location
			//that was calculated above
			FTransform Transform; Transform.SetLocation(DropLocation);

			//Default actor spawn parameters
			FActorSpawnParameters SpawnParams;

			//Spawning our pickup
			ABasePickup* PickupToSpawn = GetWorld()->SpawnActor<ABasePickup>(CurrentlyEquippedItem->GetClass(), Transform, SpawnParams);


			if (PickupToSpawn)
			{
				//Unreferenced the item we've just placed
				Inventory[IndexOfItem] = nullptr;
			}
		}
	}
}

ABaseWeapon * AMainCharacter::SpawnWeapon()
{
	if (WeaponToSpawn_Class)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		UE_LOG(LogTemp, Warning, TEXT("Trying to spawn weapon"));
		return Cast<ABaseWeapon>(GetWorld()->SpawnActor(WeaponToSpawn_Class, &TmpTransform, SpawnParams));
	}

	return nullptr;
}

void AMainCharacter::AttachWeaponToThigh(FName SocketName)
{
	if (ThighWeapon)
	{
		ThighWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
	}
}

void AMainCharacter::AttachWeaponToHand(FName SocketName)
{
	if (ThighWeapon)
	{
		ThighWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
	}
}

void AMainCharacter::Raycast()
{
	FVector StartLocation = CameraComponent->GetComponentLocation();
	FVector EndLocation = StartLocation + (CameraComponent->GetForwardVector() * RaycastRange);

	FHitResult RaycastHit;

	FCollisionQueryParams CQP;
	CQP.AddIgnoredActor(this);

	GetWorld()->LineTraceSingleByChannel(RaycastHit, StartLocation, EndLocation, ECollisionChannel::ECC_WorldDynamic, CQP);

	ABasePickup* Pickup = Cast<ABasePickup>(RaycastHit.GetActor());

	if (LastItemSeen && LastItemSeen != Pickup)
	{
		LastItemSeen->SetGlowEffect(false);
	}

	if (Pickup)
	{
		LastItemSeen = Pickup;
		Pickup->SetGlowEffect(true);
	}
	else
	{
		LastItemSeen = nullptr;
	}
}



