// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "InventoryWidget.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class THELASTFRONTIER_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()

private:

	UInventoryWidget* InventoryWidgetRef;

	bool bIsInventoryOpen;

protected:

	UPROPERTY(EditDefaultsOnly, Category = "My Player Controller")
		TSubclassOf<UInventoryWidget> InventoryWidgetBP;

public:

	virtual void OnPossess(APawn* InPawn) override;

	void HandleInventoryInputController();
	
};
