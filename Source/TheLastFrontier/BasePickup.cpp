// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePickup.h"
#include <Components/StaticMeshComponent.h>

// Sets default values
ABasePickup::ABasePickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PickupSM = CreateDefaultSubobject<UStaticMeshComponent>(FName("PickupSM"));

	PickupTexture = CreateDefaultSubobject<UTexture2D>(FName("ItemTexture"));

}

void ABasePickup::SetGlowEffect(bool Status)
{
	PickupSM->SetRenderCustomDepth(Status);
}

// Called when the game starts or when spawned
void ABasePickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasePickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

