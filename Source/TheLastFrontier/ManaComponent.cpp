// Fill out your copyright notice in the Description page of Project Settings.


#include "ManaComponent.h"

// Sets default values for this component's properties
UManaComponent::UManaComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


float UManaComponent::GetMaxMana()
{
	return MaxMana;
}

float UManaComponent::GetCurrentMana()
{
	return CurrentMana;
}

void UManaComponent::SetCurrentMana(float Mana)
{
	Mana = CurrentMana;
}

// Called when the game starts
void UManaComponent::BeginPlay()
{
	Super::BeginPlay();

	//if (GetOwner())
	//{
	//	GetOwner()->OnTakeRadialDamage.AddDynamic(this, &UManaComponent::ManaHandle);
	//}
	
}

//void UManaComponent::ManaHandle(AActor * DamagedActor, float Damage, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
//{
//	UE_LOG(LogTemp, Warning, TEXT("%s spent %s man"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));
//
//	CurrentMana = CurrentMana - Damage;
//
//	OnManaChanged.Broadcast();
//}

void UManaComponent::SubstractMana(float Mana)
{
	if (CurrentMana > 0)
	{
		float NewMana = CurrentMana - ManaToSubstractHeal;
		if (NewMana < MaxMana)
		{
			CurrentMana = NewMana;
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Something is going wrong here!"));
		}
		OnManaChanged.Broadcast();
	}
}


// Called every frame
void UManaComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

