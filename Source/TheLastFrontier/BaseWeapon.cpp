// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseWeapon.h"
#include <Engine/StaticMeshActor.h>
#include <MessageDialog.h>
#include <LogMacros.h>
#include "ImpactEffectComponent.h"
#include "ImpactEffectActor.h"
#include <Components/StaticMeshComponent.h>
#include <Components/CapsuleComponent.h>

ABaseWeapon::ABaseWeapon()
{
	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);

	//GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));

	//Setting Sword Collision Capsule
	SwordCollisionCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("SwordCollisionCapsule"));
	SwordCollisionCapsule->SetupAttachment(RootComponent);
	SwordCollisionCapsule->SetCollisionProfileName("NoCollision");

	ImpactEffectComponent = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComponent"));
}

void ABaseWeapon::StartAttack()
{
}

void ABaseWeapon::StopAttack()
{
}

void ABaseWeapon::WeaponCollisionOn()
{
	UE_LOG(LogTemp, Warning, TEXT("WeaponCollisionOn"));
	SwordCollisionCapsule->SetCollisionProfileName("Weapon");
}

void ABaseWeapon::WeaponCollisionOff()
{
	UE_LOG(LogTemp, Warning, TEXT("WeaponCollisionOff"));
	SwordCollisionCapsule->SetCollisionProfileName("NoCollision");
}

void ABaseWeapon::Attack()
{
	OnHitAttack.BindDynamic(ImpactEffectComponent, &UImpactEffectComponent::SpawnImpactEffect);
}

void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void ABaseWeapon::NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	if (OnHitAttack.IsBound())
	{
		OnHitAttack.Execute(Hit);
	}
}


