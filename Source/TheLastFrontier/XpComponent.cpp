// Fill out your copyright notice in the Description page of Project Settings.


#include "XpComponent.h"

// Sets default values for this component's properties
UXpComponent::UXpComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


float UXpComponent::GetMaxXP()
{
	return MaxXP;
}

float UXpComponent::GetCurrentXP()
{
	return CurrentXP;
}

int32 UXpComponent::GetCurrentLevel()
{
	return CurrentLevel;
}

void UXpComponent::SetCurrentXP(float XP)
{
	XP = CurrentXP;
}

void UXpComponent::AddXP(float XP)
{
	if (CurrentXP != MaxXP)
	{
		float NewXP = CurrentXP + XPForKillingMelee;
		if (NewXP < MaxXP)
		{
			CurrentXP = NewXP;
		}
		else {
			CurrentXP = MaxXP;
			CurrentLevel = CurrentLevel + 1;
			CurrentXP = 0;
			MaxXP = MaxXP * 1.5;
		}
		OnXPChanged.Broadcast();
		UE_LOG(LogTemp, Warning, TEXT("AddXp function in XPComp went off"));
	}
}

// Called when the game starts
void UXpComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UXpComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

