// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BasePickup.h"
#include "InventoryWidget.generated.h"

/**
 * 
 */
UCLASS()
class THELASTFRONTIER_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
		void Show();

	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
		void Hide();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<ABasePickup*> ItemsArray;
	
};
