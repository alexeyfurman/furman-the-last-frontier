// Fill out your copyright notice in the Description page of Project Settings.


#include "MeleeGuardCharacter.h"
#include "MainCharacter.h"
#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/Character.h>
#include <Kismet/GameplayStatics.h>

//AMeleeGuardCharacter::AMeleeGuardCharacter()
//{
//	//VitalityComponent->OnOwnerDied.AddDynamic(this, &AMeleeGuardCharacter::CharacterDied);
//
//	/*if (VitalityComponent->GetCurrentHealth() == 0)
//	{
//		MeleeGuardDied();
//		UE_LOG(LogTemp, Warning, TEXT("Warning in the MG constructor went off"));
//	}*/
//}

void AMeleeGuardCharacter::BeginPlay() 
{
	Super::BeginPlay();

	AttachWeaponToHand(WeaponSocket);
}

void AMeleeGuardCharacter::CharacterDied()
{
	Super::CharacterDied();
	AMainCharacter* Char = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Char)
	{
		Char->XpComponent->AddXP(XPForKillingMelee);
	}
	UE_LOG(LogTemp, Warning, TEXT("Melee Guard Died function in MeleeGuard worked"));
}

void AMeleeGuardCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}