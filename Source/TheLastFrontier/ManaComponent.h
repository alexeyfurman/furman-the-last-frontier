// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ManaComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegateMana);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THELASTFRONTIER_API UManaComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UManaComponent();

	float GetMaxMana();
	float GetCurrentMana();
	void SetCurrentMana(float Mana);

	UFUNCTION()
	void SubstractMana(float Mana);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	/*UFUNCTION()
		void ManaHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);*/

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Mana")
		float MaxMana = 100;

	UPROPERTY(BlueprintReadWrite, Category = "Mana")
		float CurrentMana = MaxMana;

	UPROPERTY(EditDefaultsOnly, Category = "Heal")
		float ManaToSubstractHeal = 20.0f;

	UPROPERTY(BlueprintAssignable, EditDefaultsOnly, Category = "Delegates")
		FNoParamDelegateMana OnManaChanged;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
