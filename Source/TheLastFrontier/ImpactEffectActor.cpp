// Fill out your copyright notice in the Description page of Project Settings.


#include "ImpactEffectActor.h"
#include "Kismet/GameplayStatics.h"
#include <GameFramework/Character.h>
#include <GameFramework/CharacterMovementComponent.h>

// Sets default values
AImpactEffectActor::AImpactEffectActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AImpactEffectActor::BeginPlay()
{
	Super::BeginPlay();

	SpawnEffects();
	
}

void AImpactEffectActor::HitInit(FHitResult Hit)
{
	EffectHit = Hit;
}

void AImpactEffectActor::SpawnEffects()
{
	if (DecalMaterial)
	{
		FRotator DecalRotation = FRotationMatrix::MakeFromX(EffectHit.ImpactNormal).Rotator();

		float RandomRotation = FMath::FRandRange(-180, 180);
		UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(4, 8, 8), EffectHit.GetComponent(), EffectHit.BoneName, EffectHit.ImpactPoint, DecalRotation, EAttachLocation::KeepWorldPosition, 5);
	}

	if (EffectSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, EffectSound, EffectHit.ImpactPoint);
	}

	if (EffectParticle)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, EffectParticle, EffectHit.ImpactPoint, FRotator::ZeroRotator);
	}

	if (bApplyImpulse)
	{
		if (EffectHit.GetComponent()->Mobility == EComponentMobility::Movable)
		{
			if (Cast<ACharacter>(EffectHit.GetActor()))
			{
				Cast<ACharacter>(EffectHit.GetActor())->GetCharacterMovement()->AddImpulse((EffectHit.ImpactPoint - GetOwner()->GetActorLocation().GetSafeNormal()) * ImpulseStrength, true);
			}

			else if (GetOwner())
			{
				EffectHit.GetComponent()->AddImpulse((EffectHit.ImpactPoint - GetOwner()->GetActorLocation().GetSafeNormal()) * ImpulseStrength);
			}

			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Owner is empty"));
			}
		}
	}

	if (EffectHit.GetActor())
	{
		UGameplayStatics::ApplyDamage(EffectHit.GetActor(), Damage, nullptr, GetOwner(), nullptr);
	}
}

// Called every frame
void AImpactEffectActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

