// Fill out your copyright notice in the Description page of Project Settings.


#include "AttackStartNotifyState.h"
#include "MainCharacter.h"
#include <Engine/Engine.h>
#include <Components/SkeletalMeshComponent.h>

void UAttackStartNotifyState::NotifyBegin(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float TotalDuration)
{
	//GEngine->AddOnScreenDebugMessage(-1, 4.5f, FColor::Magenta, __FUNCTION__);

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL) {
		AMainCharacter* Player = Cast<AMainCharacter>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			Player->KickStart();
		}
	}
}

void UAttackStartNotifyState::NotifyEnd(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	//GEngine->AddOnScreenDebugMessage(-1, 4.5f, FColor::Magenta, __FUNCTION__);

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL) {
		AMainCharacter* Player = Cast<AMainCharacter>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			Player->KickEnd();
		}
	}
}

