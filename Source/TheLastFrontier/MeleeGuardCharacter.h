// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MainCharacter.h"
#include "MeleeGuardCharacter.generated.h"

/**
 * 
 */
UCLASS()
class THELASTFRONTIER_API AMeleeGuardCharacter : public AMainCharacter
{
	GENERATED_BODY()

public:

	//Sets default values for this character's properties
		/*AMeleeGuardCharacter();*/

protected:

		virtual void CharacterDied() override;

	UPROPERTY(EditDefaultsOnly, Category = "XP for killing")
		float XPForKillingMelee = 25.0f;

		virtual void BeginPlay() override;

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};
