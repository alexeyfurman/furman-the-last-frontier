// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include <DelegateCombinations.h>
#include "XpComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegateXP);



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THELASTFRONTIER_API UXpComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UXpComponent();

	float GetMaxXP();
	float GetCurrentXP();
	int32 GetCurrentLevel();
	void SetCurrentXP(float XP);

	UFUNCTION(BlueprintCallable)
		void AddXP(float XP);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "XP")
		float MinXP = 0;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "XP")
		float MaxXP = 100;

	UPROPERTY(BlueprintReadWrite, Category = "XP")
		float CurrentXP = MinXP;

	UPROPERTY(BlueprintReadWrite, Category = "XP")
		int32 CurrentLevel = 1;

	UPROPERTY(EditDefaultsOnly, Category = "XP")
		float XPForKillingMelee = 20.0f;

	UPROPERTY(BlueprintAssignable, EditDefaultsOnly, Category = "Delegates")
		FNoParamDelegateXP OnXPChanged;

	UPROPERTY(EditDefaultsOnly)
		int32 Level = 0;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
