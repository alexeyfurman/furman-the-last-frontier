// Fill out your copyright notice in the Description page of Project Settings.


#include "HealEndNotifyState.h"
#include <Engine/Engine.h>
#include "VitalityComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MainCharacter.h"

void UHealEndNotifyState::NotifyBegin(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float TotalDuration)
{
	//GEngine->AddOnScreenDebugMessage(-1, 4.5f, FColor::Magenta, __FUNCTION__);

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL) {
		AMainCharacter* Player = Cast<AMainCharacter>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			Player->HealStart();
		}
	}
}

void UHealEndNotifyState::NotifyEnd(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	//GEngine->AddOnScreenDebugMessage(-1, 4.5f, FColor::Magenta, __FUNCTION__);

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL) {
		AMainCharacter* Player = Cast<AMainCharacter>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			Player->HealEnd();
		}
	}
}

