// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ManaComponent.h"
#include "Components/BoxComponent.h"
#include <Particles/ParticleSystem.h>
#include "BasePickup.h"
#include "XpComponent.h"
#include "VitalityComponent.h"
#include "MainCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UCharacterMovementComponent;

UCLASS()
class THELASTFRONTIER_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

private:

	UFUNCTION()
		void Raycast();

	ABasePickup* LastItemSeen;

	ABasePickup* CurrentlyEquippedItem;

public:
	// Sets default values for this character's properties
	AMainCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	UFUNCTION()
		void KickStart();

	UFUNCTION()
		void KickEnd();

	UFUNCTION()
		void HealStart();

	UFUNCTION()
		void HealEnd();

	UFUNCTION()
		void WeaponCollisionOnCharacter();

	UFUNCTION()
		void WeaponCollisionOffCharacter();

	void SetEquippedItem(UTexture2D* Texture);

	UFUNCTION(BlueprintCallable)
		void DropEquippedItem();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UXpComponent* XpComponent;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void MoveForward(float AxisValue);

	UFUNCTION()
		void MoveRight(float AxisValue);

	UFUNCTION()
		void SecondaryAttack();

	UFUNCTION()
		void Attack();

	UFUNCTION()
		void KickInput();

	UFUNCTION()
		void HealInput();

	UFUNCTION()
		void StartRun();

	UFUNCTION()
		void StopRun();

	UFUNCTION()
		void EquipWeapon();

	UFUNCTION()
		void PickupItem();

	UFUNCTION()
		virtual void CharacterDied();

	UFUNCTION()
		void HandleInventoryInput();

	bool bWeaponIsEquipped = false;

	class ABaseWeapon* SpawnWeapon();

	void AttachWeaponToThigh(FName SocketName);

	void AttachWeaponToHand(FName SocketName);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UVitalityComponent* VitalityComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UManaComponent* ManaComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCharacterMovementComponent* CharacterMovementComponent;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		TSubclassOf<class ABaseWeapon> WeaponToSpawn_Class;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName WeaponSocket;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName ThighSocket;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class ABaseWeapon* CurrentWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class ABaseWeapon* ThighWeapon;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* AttackMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* SecondaryAttackMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* EquipMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* HolsterMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* KickMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* HealMontage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Collision")
		class UBoxComponent* RightFootCollisionBox;

	UPROPERTY(EditDefaultsOnly, Category = "Heal")
		float HealthHealAmount = 30.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Heal")
		float ManaToHealAmount = 30.0f;

	/*UPROPERTY(EditDefaultsOnly, Category = "Heal")
		float MaxHealthToHeal = 80.0f;*/

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float RunSpeed = 1200.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float WalkSpeed = 600.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float XPForKill = 30.0f;

	UPROPERTY(EditAnywhere)
		float RaycastRange = 500.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
		int32 MaxInventoryItems = 4;

	UPROPERTY(VisibleAnywhere)
		TArray<ABasePickup*> Inventory;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	TArray<ABasePickup*> GetInventory() { return Inventory; }

	//TSubclassOf<ABaseWeapon*> GetWeapon() { return WeaponToSpawn_Class; }

};
