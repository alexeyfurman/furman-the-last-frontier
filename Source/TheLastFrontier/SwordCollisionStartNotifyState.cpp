// Fill out your copyright notice in the Description page of Project Settings.


#include "SwordCollisionStartNotifyState.h"
#include "BaseWeapon.h"
#include <Engine/Engine.h>
#include <Components/SkeletalMeshComponent.h>
#include "MainCharacter.h"

void USwordCollisionStartNotifyState::NotifyBegin(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float TotalDuration)
{
	//GEngine->AddOnScreenDebugMessage(-1, 4.5f, FColor::Magenta, __FUNCTION__);

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL) {
		AMainCharacter* Player = Cast<AMainCharacter>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			Player->WeaponCollisionOnCharacter();
		}
	}
}

void USwordCollisionStartNotifyState::NotifyEnd(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	//GEngine->AddOnScreenDebugMessage(-1, 4.5f, FColor::Magenta, __FUNCTION__);

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL) {
		AMainCharacter* Player = Cast<AMainCharacter>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			Player->WeaponCollisionOffCharacter();
		}
	}

}

