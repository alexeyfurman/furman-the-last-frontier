// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"
#include "MainCharacter.h"
#include "GameFramework/Controller.h"

void AMyPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (InventoryWidgetBP)
	{
		InventoryWidgetRef = CreateWidget<UInventoryWidget>(this, InventoryWidgetBP);
	}

	bIsInventoryOpen = false;
}

void AMyPlayerController::HandleInventoryInputController()
{
	AMainCharacter* Char = Cast<AMainCharacter>(GetPawn());

	if (InventoryWidgetRef)
	{
		if (bIsInventoryOpen)
		{
			bIsInventoryOpen = false;

			InventoryWidgetRef->RemoveFromViewport();

			//bShowMouseCursor = false;

			//FInputModeGameOnly InputMode1;
			//SetInputMode(InputMode1);

			//SetPause(false);
		}
		else
		{
			bIsInventoryOpen = true;

			InventoryWidgetRef->ItemsArray = Char->GetInventory();

			InventoryWidgetRef->Show();

			//bShowMouseCursor = true;

			//FInputModeGameAndUI InputMode2;
			//SetInputMode(InputMode2);

			//SetPause(true);
		}
	}
}
