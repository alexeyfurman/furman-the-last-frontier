// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityComponent.h"

// Sets default values for this component's properties
UVitalityComponent::UVitalityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


float UVitalityComponent::GetMaxHealth()
{
	return MaxHealth;
}

float UVitalityComponent::GetCurrentHealth()
{
	return CurrentHealth;
}

void UVitalityComponent::SetCurrentHealth(float Health)
{
	Health = CurrentHealth;
}

// Called when the game starts
void UVitalityComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UVitalityComponent::DamageHandle);
	}
	
}

void UVitalityComponent::DamageHandle(AActor * DamagedActor, float Damage, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("%s was damaged for %s"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));

	if (!bIsAlive())
	{
		UE_LOG(LogTemp, Warning, TEXT("AlreadyDead"));
		return;
	}

	CurrentHealth = CurrentHealth - Damage;

	OnHealthChanged.Broadcast();

	if (!bIsAlive())
	{
		//Died
		OnOwnerDied.Broadcast();
		//UMyGameInstance* MyInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
		UE_LOG(LogTemp, Warning, TEXT("OwnerDied"));   
	}
}

void UVitalityComponent::AddHealth(float Health)
{
	if (CurrentHealth > 0 && CurrentHealth != MaxHealth)
	{
		float NewHealth = CurrentHealth + HealthToAdd;
		if (NewHealth < MaxHealth)
		{
			CurrentHealth = NewHealth;
		}
		else
		{
			CurrentHealth = MaxHealth;
		}
		OnHealthChanged.Broadcast();
	}
}

bool UVitalityComponent::bIsAlive()
{
	return CurrentHealth > 0;
}

// Called every frame
void UVitalityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


