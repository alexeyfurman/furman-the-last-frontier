// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VitalityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THELASTFRONTIER_API UVitalityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVitalityComponent();

	float GetMaxHealth();
	float GetCurrentHealth();
	void SetCurrentHealth(float Health);

	UFUNCTION()
		void AddHealth(float Health);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
		void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Health")
		float MaxHealth = 100;

	UPROPERTY(BlueprintAssignable, EditDefaultsOnly, Category = "Delegates")
		FNoParamDelegate OnHealthChanged;

	UPROPERTY(BlueprintReadWrite, Category = "Health")
		float CurrentHealth = MaxHealth;

	UPROPERTY(EditDefaultsOnly, Category = "Heal")
		float HealthToAdd = 20.0f;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		bool bIsAlive();
	
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegate OnOwnerDied;
};
